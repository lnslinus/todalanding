<?php namespace Linus\Forms;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
      return [
       '\Linus\Forms\Components\Contact' => 'Contact',
     ];
    }

    public function registerSettings()
    {
    }
}
