<?php namespace Linus\Forms\Models;

use Model;

/**
 * Model
 */
class Contact extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'linus_forms_';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
