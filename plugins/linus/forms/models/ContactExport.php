<?php namespace Linus\Forms\Models;

use Backend\Models\ExportModel;

/**
 * Model
 */
class ContactExport extends ExportModel
{
  public function exportData($columns, $sessionKey = null)
 {
     $contact = Contact::all();
     $contact->each(function($contact) use ($columns) {
         $contact->addVisible($columns);
     });

     return $contact->toArray();
 }
}
