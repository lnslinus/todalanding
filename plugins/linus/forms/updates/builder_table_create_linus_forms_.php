<?php namespace Linus\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLinusForms extends Migration
{
    public function up()
    {
        Schema::create('linus_forms_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 255);
            $table->string('email', 255);
            $table->string('contact', 255);
            $table->date('birthdate');
            $table->string('subject', 255);
            $table->text('message');
            $table->integer('order_id')->default(0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('linus_forms_');
    }
}
