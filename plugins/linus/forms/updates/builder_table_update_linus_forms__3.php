<?php namespace Linus\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLinusForms3 extends Migration
{
    public function up()
    {
        Schema::table('linus_forms_', function($table)
        {
            $table->dropColumn('contact');
            $table->dropColumn('birthdate');
            $table->dropColumn('subject');
            $table->dropColumn('order_id');
        });
    }
    
    public function down()
    {
        Schema::table('linus_forms_', function($table)
        {
            $table->timestamp('created_at')->default('NULL')->change();
            $table->timestamp('updated_at')->default('NULL')->change();
            $table->timestamp('deleted_at')->default('NULL')->change();
            $table->string('contact', 255);
            $table->string('birthdate', 50)->nullable()->default('NULL');
            $table->string('subject', 255);
            $table->integer('order_id')->default(0);
        });
    }
}
