<?php namespace Linus\Forms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLinusForms extends Migration
{
    public function up()
    {
        Schema::table('linus_forms_', function($table)
        {
            $table->string('birthdate', 50)->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('linus_forms_', function($table)
        {
            $table->date('birthdate')->nullable(false)->unsigned(false)->default(null)->change();
            $table->timestamp('created_at')->default('NULL')->change();
            $table->timestamp('updated_at')->default('NULL')->change();
            $table->timestamp('deleted_at')->default('NULL')->change();
        });
    }
}
