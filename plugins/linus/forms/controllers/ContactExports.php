<?php namespace Linus\Forms\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class ContactExports extends Controller
{
	public $implement = [ 'Backend\Behaviors\ImportExportController'];

    public $importExportConfig = 'config_export_contact.yaml';

    public $requiredPermissions = [
        'Contact'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Linus.Forms', 'main-menu-item2', 'side-menu-item3');
    }
}
