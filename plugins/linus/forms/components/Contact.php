<?php namespace Linus\Forms\Components;

use Cms\Classes\ComponentBase;
use Input;
use Validator;
use ValidationException;
use Redirect;
use Mail;
use Flash;
use Linus\Forms\Models\Contact as ContactModel;

class Contact extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Contact',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onSendForm(){
        // Validation
        $data = post();
        $rules = [
            'fullname' => 'required',
            'email' => 'required|email',
            'message' => 'required',
        ];
        $validator = Validator::make($data, $rules);

        if($validator->fails()){
            throw new ValidationException($validator);
        }
        else{
            // Form Data
            $fullname = Input::get('fullname');
            $email = Input::get('email');
            $message = Input::get('message');

            // Add to DB
            $serv = new ContactModel();
            $serv->name = $fullname;
            $serv->email = $email;
            $serv->message = $message;
            $serv->save();

            //Send Email Notification
            $vars = [
                'vars' => $data,
            ];

            Mail::send('contactform', $vars, function($message){
                $message->to('harold@webworx.asia', "TODA PH");
                //$message->to('matthew.rufino@gmail.com', "Town's Delight - The Caterer");
                $message->subject('Contact Form | You have a new inquiry');
            });

            Mail::send('autorespond', $vars, function($message){
                $message->to( Input::get('email'), Input::get('fullname'));
                $message->subject('Thank you for filling out!');
            });
        }
    }
}
