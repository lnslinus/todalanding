<?php namespace Linus\Contact\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLinusContact extends Migration
{
    public function up()
    {
        Schema::create('linus_contact_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('address', 255);
            $table->string('email', 150);
            $table->string('fb', 255);
            $table->string('ig', 255);
            $table->string('tw', 255);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('linus_contact_');
    }
}
