<?php namespace Linus\Contact\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLinusContact extends Migration
{
    public function up()
    {
        Schema::table('linus_contact_', function($table)
        {
            $table->string('map', 255);
        });
    }
    
    public function down()
    {
        Schema::table('linus_contact_', function($table)
        {
            $table->dropColumn('map');
            $table->timestamp('created_at')->default('NULL')->change();
            $table->timestamp('updated_at')->default('NULL')->change();
            $table->timestamp('deleted_at')->default('NULL')->change();
        });
    }
}
