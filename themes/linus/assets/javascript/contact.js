
    $('.form-input-text').each(function(e){
        $(this).on('keyup', function(e){
          if($(this).val().length > 0 ){
            $(this).removeClass('has-error');
            $(this).closest('.field-set').removeClass('has-error');
          }else{
            $(this).closest('.field-set').addClass('has-error');
            $(this).toggleClass('has-error');
          }
        });
    });
    $(window).on('ajaxInvalidField', function(event, fieldElement, fieldName, errorMsg, isFirst) {
    $(fieldElement).closest('.form-input-text').addClass('has-error');
    $(fieldElement).closest('.field-set').addClass('has-error');
    console.log("Invalid");
  });
  $(document).on('ajaxPromise', '[data-request]', function() {
    $(this).closest('form').find('.form-input-text.has-error').removeClass('has-error');
    $(this).closest('form').find('.field-set.has-error').removeClass('has-error');
  });
