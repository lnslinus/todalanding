
$(document).ready(function(){
  
  if ($(window).width() > 480) {
  var controller = new ScrollMagic.Controller();
         var scene = new ScrollMagic.Scene({
           triggerElement: "#vuumpet",
           triggerHook: 'onLeave',
           duration: 0,
           offset: -90
       })
       .setPin(".pin-nav")
       // .addIndicators()
       .addTo(controller);
}


  $('.prod-slider').slick({
    speed: 300,
    slidesToShow: 1,
    centerMode: false,
    arrows: false,
    draggable: false,
    swipe: false,
    adaptiveHeight: true,
  });
  $('.slider-nav').slick({
    speed: 300,
    slidesToShow: 1,
    centerMode: false,
    arrows: false,
    variableWidth: true,
    swipe: false,
    draggable: false,
  });
  $('.slider-nav').slick('slickGoTo', 1);
  $(".slick-arrow").click(function(e){
          e.preventDefault();
          slideIndex = $(this).index();
          $( '.slider-nav' ).slickGoTo( parseInt(slideIndex) );
      });
  $('.prod-next-btn').click(function(){
    $('.prod-slider').slick('slickNext');
    $('.slider-nav').slick('slickNext');
  });
});

$('.prod-dets-slider').slick({
  arrows:false,
  speed: 300,
  slidesToShow: 1,
  centerMode: false,
  infinite: true,
  dots: true,
});
  $('.modal').on('show.bs.modal', function () {
      $(window).resize();
      $('.prod-dets-slider').slick('refresh');
  });

$(document).ready(function() {

	$('.image-popup-vertical-fit').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile',
		image: {
			verticalFit: true
		}

	});

	$('.image-popup-fit-width').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		image: {
			verticalFit: false
		}
	});

	$('.image-popup-no-margins').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		closeBtnInside: false,
		fixedContentPos: true,
		mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
		image: {
			verticalFit: true
		},
		zoom: {
			enabled: true,
			duration: 300 // don't foget to change the duration also in CSS
		}
	});
  // jQuery("#vuumpet-gall").unitegallery({
  //   tiles_space_between_cols: 25,
  //   tiles_max_columns: 2
  // });
  // jQuery("#poppet-gall").unitegallery({
  //   tiles_space_between_cols: 25,
  //   tiles_max_columns: 2
  // });
  $(".prod-gall").each(function(){
    $(this).unitegallery({
      tiles_space_between_cols: 25,
      tiles_max_columns: 2
    });
  });
});

$(document).ready(function() {
	$('.popup-vimeo').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,
		fixedContentPos: false
	});
});
